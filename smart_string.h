#pragma once

#include <iostream>

using namespace std;

class sm_string {

public:
	void show();
	sm_string();
	~sm_string();
	void append_data(char);
	char operator[](int);
	int length();
private:
	int _length;
	char* data;
	int capacity;
};

sm_string::sm_string() 
{
	data = new char;
	capacity = 1;
	_length = 0;
}

sm_string::~sm_string()
{
	delete [] data;
}

void sm_string::append_data(char str)
{
	while (_length + 1 >= capacity) 
	{
		capacity *= 2;
		char* temp = data;
		data = new char[capacity];
		for (int i = 0; i < _length; i++)
		{
			data[i] = temp[i];
		}

		delete[] temp;
	}

	data[_length] = str;
	_length++;
}

int sm_string::length()
{
	return _length;
}

char sm_string::operator[](int index)
{
	if (index < _length)
		return data[index];
	else
		cout <<endl << "index out of bound" << endl;
}

void sm_string::show()
{
	for (int i = 0; i < _length; i++)
	{
		cout << data[i];
	}
}